// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require_tree .

$(function(){
    if ($("#comments").length > 0) {
        setTimeout(updateComments, 5000);
    }
})

function updateComments() {
    var article_id = $("#article").attr("data-id");
    var after = $(".comment:last-child").attr("data-time");
    if ($(".comment").length > 0) {
        var after = $(".comment:last-child").attr("data-time");
    } else {
        var after = "0";
    }

    $.getScript(article_id + "/comments.js?article_id=" + article_id + "&after=" + after);
    setTimeout(updateComments, 10000)
}

