class CommentsController < ApplicationController

  def index
    @comments = Comment.where('article_id = ? and created_at > ?', params[:article_id], Time.at(params[:after].to_i + 1))
  end

  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  private
  def comment_params
    params.require(:comment).permit(:commenter, :body)
  end

end
